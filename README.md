# puppeteer-vrec

Puppeteer video recorder is a simple Puppeteer's plugin to create automatic videos for every new frame appears in the browser.

## Why this Fork?

This library is based on Shay Gueta's <a href="https://github.com/shaynet10/puppeteer-video-recorder">puppeteer-video-recorder</a> and <a href="https://www.npmjs.com/package/puppeteer-mass-screenshots">puppeteer-mass-screenshots</a>.

I was missing some features that, fortunately others submitted in PRs. Unfortunately, those did not get merged in, neither equivalent functionality was provided over several months. I decided therefore it was best to fork the project and just maintain my own version of it and add features that I need. This library also merged these two aforementioned libraries into a single video recording library.

## Prerequisites

In order to use this plugin:

<ul>
    <li>Puppeteer must be installed.</li>
    <li>Pupepteer's page object must be created.</li>
    <li><b>FFMpeg must be installed</b> and set in PATH. (or change the ffmpeg command to where it's accessible from)</li>
</ul>

## Installation

To install the plugin to your project please use:

```javascript
npm install --save puppeteer-vrec
```

## Manual

Once Puppeteer video recorder is installed, you can require it in your project:

```javascript
const PuppeteerVideoRecorder = require('puppeteer-vrec');

const recorder = new PuppeteerVideoRecorder(page);
await recorder.start();
// Do whatever you want in the browser here ...
const filePath = await recorder.stop();
// filePath is the location of your video or `null` if there was an error while creating the video.
```

Check out `tests/index.test.js` for a simple example.

You can pass in custom configuration via the constructor, for example:

```javascript
const recorder = new PuppeteerVideoRecorder(page, {
    outputFolder: '/tmp',
    videoFilename: 'test-video'
});
```

The configuration options are:

<ul> 
    <li><b>outputFolder</b> - Where to save the created videos, images file and temporary images. The default location is the operating system's default temporary directory.</li>
    <li><b>videoFilename</b> - The name of the final video file. (with .wbem appended as extension) The default file name is the current timestamp.</li>
</ul>

## FAQ

### Does it support Chrome in headless mode?

Yes, it supports Chrome in both headless and headful mode.

<br/>

### Does it support redirections in pages?

Yes. This plugin is based on  <a href="https://www.npmjs.com/package/puppeteer-mass-screenshots">Puppeteer mass screenshots plugin</a> which supports redirection.

<br/>

### Does it use the window object?

No, it doesn't use the window object.

<br/>

### Can I run this plugin with my own page/browser objects?

Yes.

<br/>

### Will it change my browser/page/window objects?

No, it won't. Feel free to set browser/page/window as you like, it won't be affected by this plugin.

<br/>

### Creating videos is slow why?

It is because current FFMPEG command is slow.

We should in the future upload a solution for faster conversion of FFMPEG. Feel free to send us one if you've found something faster and better, we always love to improve.

<br/>

### I get errors related to FFMPEG, why?

Please ensure that you have FFMPEG installed on your PC.

run from your cmd / terminal (in linux) the command: 

```
ffmpeg --help
```

If you see results, and don't see "command not found" errors, this plugin should work with your FFMPEG.