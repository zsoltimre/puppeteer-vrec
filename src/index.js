const { execSync } = require('child_process');

const { FsHandler } = require('./handlers');
const Screenshots = require('./Screenshots');

class PuppeteerVideoRecorder {
    constructor(page, config = {}) {
        this.page = page;
        this.fs = new FsHandler(config);
        const { imagesPath, imagesFilename } = this.fs;
        this.screenshots = new Screenshots(this.page, this.fs.imagesPath, {
            afterWritingImageFile: (filename) =>
                FsHandler.appendToFile(imagesFilename, `file '${filename}'\n`),
        });
    }

    start(options = {}) {
        return this.screenshots.start(options);
    }

    async stop() {
        await this.screenshots.stop();
        return this.createVideo();
    }

    get FFMpegCommand() {
        const { imagesFilename, videoFilename } = this.fs;
        return [
            'ffmpeg',
            '-f concat',
            '-safe 0',
            `-i ${imagesFilename}`,
            '-framerate 60',
            videoFilename,
        ].join(' ');
    }

    createVideo() {
        const { videoFilename } = this.fs;
        try {
            execSync(this.FFMpegCommand);
            return videoFilename;
        } catch (e) /* istanbul ignore next */ {
            console.error(e);
            return null;
        }
    }
}

module.exports = PuppeteerVideoRecorder;
