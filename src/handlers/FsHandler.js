const {
    openSync,
    closeSync,
    existsSync,
    unlinkSync,
    mkdirSync,
    appendFileSync,
    rmdirSync,
    writeFileSync,
} = require('fs');
const { tmpdir } = require('os');
const { join } = require('path');

class FsHandler {
    constructor(config = {}) {
        const { outputFolder, videoFilename } = config;
        this.outputFolder = outputFolder || tmpdir();
        const fileName = videoFilename || Date.now();
        this.videoFilename = join(this.outputFolder, fileName + '.webm');
        this.imagesPath = join(this.outputFolder, 'images');
        this.imagesFilename = join(this.outputFolder, 'images.txt');

        rmdirSync(this.imagesPath, { recursive: true });
        if (existsSync(this.videoFilename)) {
            unlinkSync(this.videoFilename);
        }

        FsHandler.ensureExists(this.outputFolder);
        FsHandler.ensureExists(this.imagesPath);
        FsHandler.createFsAsset(this.imagesFilename, 'file');
    }

    static createFsAsset(path, type = 'folder') {
        if (type === 'folder') return mkdirSync(path);
        return closeSync(openSync(path, 'w'));
    }

    static ensureExists(path, type = 'folder') {
        return existsSync(path) || FsHandler.createFsAsset(path, type);
    }

    static appendToFile(filename, data) {
        return appendFileSync(filename, data);
    }

    static writeImage(outputFolder, data, cbAfterWritingImageFile = null) {
        const filename = join(outputFolder, Date.now().toString() + '.jpg');
        writeFileSync(filename, data, 'base64');
        if (cbAfterWritingImageFile) cbAfterWritingImageFile(filename);
        return filename;
    }
}

module.exports = FsHandler;
