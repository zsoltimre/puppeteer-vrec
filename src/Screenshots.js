const { FsHandler } = require('./handlers');

class Screenshots {
    constructor(page, outputFolder, options) {
        this.page = page;
        this.options = options;
        this.canScreenshot = true;
        this.outputFolder = outputFolder;
    }

    handleScreencastFrame = async (outputFolder, frameObject) => {
        if (!this.canScreenshot) return;
        FsHandler.writeImage(
            outputFolder,
            frameObject.data,
            this.options.afterWritingImageFile
        );
        try {
            await this.client.send('Page.screencastFrameAck', {
                sessionId: frameObject.sessionId,
            });
        } catch (e) /* istanbul ignore next */ {
            this.canScreenshot = false;
        }
    };

    async start(options = {}) {
        this.client = await this.page.target().createCDPSession();
        this.client.on('Page.screencastFrame', (f) =>
            this.handleScreencastFrame(this.outputFolder, f)
        );
        const startOptions = {
            format: 'jpeg',
            quality: 100,
            maxWidth: 1920,
            maxHeight: 1080,
            everyNthFrame: 1,
            ...options,
        };
        return this.client.send('Page.startScreencast', startOptions);
    }

    async stop() {
        return this.client.send('Page.stopScreencast');
    }
}

module.exports = Screenshots;
