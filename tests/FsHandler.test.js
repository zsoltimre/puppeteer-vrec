const { tmpdir } = require('os');
const { join } = require('path');
const { 
    openSync,
    closeSync,
    existsSync,
    readdirSync,
    unlinkSync,
    rmdirSync,
    readFileSync
} = require('fs');

const { FsHandler } = require('../src/handlers');

test('Instantiate with defaults', () => {
    const fsh = new FsHandler();
    expect(fsh.outputFolder).toMatch(tmpdir());
    expect(fsh.videoFilename.split('/').reverse()[0]).toMatch(/[1-9]*\.webm/);
    expect(fsh.imagesPath).toEqual(join(fsh.outputFolder, 'images'));
    expect(fsh.imagesFilename).toEqual(join(fsh.outputFolder, 'images.txt'));
    expect(existsSync(fsh.videoFilename)).toBe(false);
    expect(existsSync(fsh.outputFolder)).toBe(true);
    expect(existsSync(fsh.imagesPath)).toBe(true);
    expect(readdirSync(fsh.imagesPath).length).toBe(0);
    expect(existsSync(fsh.imagesFilename)).toBe(true);
});

test('Instantiate with config', () => {
    closeSync(openSync('/tmp/test-video.webm', 'w'));
    const fsh = new FsHandler({
        outputFolder: '/tmp',
        videoFilename: 'test-video'
    });
    expect(fsh.outputFolder).toMatch('/tmp');
    expect(fsh.videoFilename.split('/').reverse()[0]).toMatch('test-video.webm');
    expect(fsh.imagesPath).toEqual('/tmp/images');
    expect(fsh.imagesFilename).toEqual('/tmp/images.txt');

    expect(existsSync(fsh.videoFilename)).toBe(false);
    expect(existsSync(fsh.outputFolder)).toBe(true);
    expect(existsSync(fsh.imagesPath)).toBe(true);
    expect(readdirSync(fsh.imagesPath).length).toBe(0);
    expect(existsSync(fsh.imagesFilename)).toBe(true);
});

test('Create file system asset', async() => {
    const testFile = '/tmp/create-empty-file.test';

    FsHandler.createFsAsset(testFile, 'file');
    expect(existsSync(testFile)).toBe(true);
    unlinkSync(testFile);
    FsHandler.createFsAsset(testFile, 'folder');
    expect(existsSync(testFile)).toBe(true);
    rmdirSync(testFile);
    FsHandler.createFsAsset(testFile);
    expect(existsSync(testFile)).toBe(true);
    rmdirSync(testFile);
});

test('Append to file', async() => {
    const testFile = '/tmp/create-empty-file.test';

    FsHandler.createFsAsset(testFile, 'file');
    FsHandler.appendToFile(testFile, 'hello');
    let content = readFileSync(testFile).toString('utf-8');
    expect(content).toEqual('hello');
    FsHandler.appendToFile(testFile, ' there');
    content = readFileSync(testFile).toString('utf-8');
    expect(content).toEqual('hello there');
    unlinkSync(testFile);
});

test('Ensure filesystem asset exists', () => {
    const testFile = '/tmp/create-empty-file.test';

    expect(existsSync(testFile)).toBe(false);
    FsHandler.ensureExists(testFile, 'file');
    expect(existsSync(testFile)).toBe(true);
    expect(FsHandler.ensureExists(testFile, 'file')).toBe(true);
    unlinkSync(testFile);

    expect(existsSync(testFile)).toBe(false);
    FsHandler.ensureExists(testFile, 'folder');
    expect(existsSync(testFile)).toBe(true);
    expect(FsHandler.ensureExists(testFile, 'folder')).toBe(true);
    rmdirSync(testFile);
});

test('Write image file', () => {
    const testDir = '/tmp/wif.test';
    FsHandler.ensureExists(testDir, 'folder');
    const fileName = FsHandler.writeImage(testDir, 'test');
    expect(fileName.startsWith(testDir)).toBe(true);
    rmdirSync(testDir, { recursive: true });
});
