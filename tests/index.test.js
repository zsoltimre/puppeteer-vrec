const { tmpdir } = require('os');

const PuppeteerVideoRecorder = require('../src/index');
const puppeteer = require('puppeteer');

jest.setTimeout(50000);

test('Create video without config', async() => {
  const browser = await puppeteer.launch({ headless: true });
  const page = (await browser.pages())[0];
  const recorder = new PuppeteerVideoRecorder(page);
  await page.goto('https://google.com');
  await recorder.start();
  const input = await page.$('input[name=q]');
  await input.type('puppetter-mass-screenshots', { delay: 250 });
  await input.press('Enter');
  const videoLocation = await recorder.stop();
  expect(videoLocation.startsWith(tmpdir())).toBe(true);
  await browser.close();
});

test('Create video with config', async() => {
  const browser = await puppeteer.launch({ headless: true });
  const page = (await browser.pages())[0];
  const recorder = new PuppeteerVideoRecorder(page, {
    outputFolder: '/tmp',
    videoFilename: 'test-video'
  });
  await page.goto('https://google.com');
  await recorder.start();
  const acceptButton = await page.$('button[id=L2AGLb]');
  await acceptButton.click();
  const input = await page.$('input[name=q]');
  await input.type('puppetter-mass-screenshots', { delay: 250 });
  await input.press('Enter');
  const videoLocation = await recorder.stop();
  expect(videoLocation).toEqual('/tmp/test-video.webm');
  await browser.close();
});
